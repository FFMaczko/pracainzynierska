﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadTestStart : MonoBehaviour
{
    void Start()
    {
        TestData.Data.loadStart = DateTime.Now;
        SceneManager.LoadScene("Test");
    }
}
