﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour
{
    public float numberOfFrames = 1000;
    void Start()
    {
        TestData.Data.loadEnd = DateTime.Now;
    }

    void Update()
    {
        float deltaTime = Time.deltaTime;
        TestData.Data.avgDelta += deltaTime;
        if (TestData.Data.minDelta > deltaTime && Time.frameCount > 20)
        {
            TestData.Data.minDelta = deltaTime;
        }
        if (TestData.Data.maxDelta < deltaTime)
        {
            TestData.Data.maxDelta = deltaTime;
        }
        if(numberOfFrames == Time.frameCount)
        {
            TestData.Data.avgDelta = TestData.Data.avgDelta / 1000.0f;
            Debug.LogError($"{TestData.Data.avgDelta}");
            Debug.LogError($"{TestData.Data.minDelta}");
            Debug.LogError($"{TestData.Data.maxDelta}");
            Debug.LogError($"{TestData.Data.loadEnd - TestData.Data.loadStart}");
        }
    }
}
