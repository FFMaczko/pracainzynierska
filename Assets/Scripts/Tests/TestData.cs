﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestData : MonoBehaviour
{
    public DateTime loadStart;
    public DateTime loadEnd;
    public float maxDelta = -10;
    public float minDelta = 10;
    public float avgDelta = 0;

    public static TestData Data;

    private void Awake()
    {
        Data = this;
        DontDestroyOnLoad(gameObject);
    }
}
