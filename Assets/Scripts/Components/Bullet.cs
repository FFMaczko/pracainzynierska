﻿// Created by Filip Mączko
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    [RequireComponent(typeof(CollidablePoint))]
    public class Bullet : MonoBehaviour
    {
        public static readonly float BulletSpeed = 8;
        public static List<Bullet> objects = new List<Bullet>();
        public int ExplosionRadius;
        public float ExplosionForce;
        public float2 StartingSpeed;
        public Color Color;
        public CollidablePoint CollidablePoint { get; private set; }
        public MoveableObject MoveableObject { get; private set; }

        [SerializeField]
        [Tooltip ("Prefab used for creating explosions")]
        private Explosion explosionPrefab;

        private void OnEnable()
        {
            CollidablePoint = GetComponent<CollidablePoint>();
            MoveableObject = GetComponent<MoveableObject>();
            objects.Add(this);
        }

        private void Start()
        {
            GetComponent<SpriteRenderer>().color = Color;
            MoveableObject.Velocity = StartingSpeed;
        }

        public void CreateExplosion()
        {
            var explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
            explosion.ExplosionForce = ExplosionForce;
            explosion.SetSize(ExplosionRadius);
        }

        private void OnDisable()
        {
            objects.Remove(this);
        }
    }
}
