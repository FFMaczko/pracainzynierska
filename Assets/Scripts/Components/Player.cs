﻿// Created by Filip Mączko
using System.Collections.Generic;
using UnityEngine;
namespace FM
{
    public class Player : MonoBehaviour
    {
        public static readonly float PlayerMovementSpeed = 5;
        public static List<Player> objects = new List<Player>();
        public MoveableObject MoveableObject;
        public CollidablePoint CollidablePoint;
        public EffectedByGravity EffectedByGravity;
        public Transform AimArrow;
        public int MaxClimingDistance = 5; //Max Climbing height must be smaller or equal to playerHeight
        public int PlayerHeight = 5;
        public Vector3 PlayerVelocity = new Vector3(5, 0, 0);
        public bool Jump = false;
        public float JumpSpeed = 5;
        public bool Grounded = false;
        public int JumpDelay = 0;
        public int ShootDelay = 0;
        public Transform Barrel;
        public Transform HeadPoint;
        public Color PlayerColor;
        public int ID;

        [SerializeField]
        [Tooltip("WeaponsPrefabs")]
        private Weapon[] weapons;

        private void Awake()
        {
            MoveableObject = GetComponent<MoveableObject>();
            CollidablePoint = GetComponentInChildren<CollidablePoint>();
            EffectedByGravity = GetComponent<EffectedByGravity>();
        }

        private void Start()
        {
            //Barrel.GetComponent<SpriteRenderer>().color = PlayerColor;
        }

        private void OnEnable()
        {
            ID = objects.Count;
            objects.Add(this);
        }

        private void OnDisable()
        {
            objects.Remove(this);
        }
    }
}
