﻿// Created by Filip Mączko
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    public class MoveableObject : MonoBehaviour
    {
        public static List<MoveableObject> objects = new List<MoveableObject>();

        /// <summary>
        /// Position on a grid in last frame.
        /// </summary>
        public float3 LastPosition;

        /// <summary>
        /// Position on grid after movement, before collision calculations.
        /// </summary>
        public float3 PredictedPosition;

        /// <summary>
        /// Velocity of an object in pixels/frame.
        /// </summary>
        public float2 Velocity = new float2(0.2f, 0);

        /// <summary>
        /// Max velocity object can achive.
        /// </summary>
        public float2 MaxVelocity = new int2(10,10);

        private void OnEnable()
        {
            LastPosition = transform.position;
            PredictedPosition = transform.position;
            objects.Add(this);
        }

        private void Start()
        {

        }

        private void OnDisable()
        {
            objects.Remove(this);
        }
    }
}
