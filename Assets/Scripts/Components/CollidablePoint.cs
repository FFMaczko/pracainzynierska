﻿// Created by Filip Mączko
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    public class CollidablePoint : MonoBehaviour
    {
        public static List<CollidablePoint> objects = new List<CollidablePoint>();
        public float3 LastPosition = default;
        public float3 CurrentPosition = default;
        public bool Collided;
        public int2 CollisionPosition = default;
        public bool UseLineCollisions;

        private void OnEnable()
        {
            LastPosition = new float3(transform.position);
            CurrentPosition = new float3(transform.position);
            objects.Add(this);
        }

        private void Start()
        {

        }

        private void OnDisable()
        {
            objects.Remove(this);
        }

        private void OnDrawGizmos()
        {
            if (UseLineCollisions)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(Utils.GridPositionToWordPosition(Utils.WordPositionToGridPosition(CurrentPosition)),
                    Utils.GridPositionToWordPosition(Utils.WordPositionToGridPosition(LastPosition)));
                Gizmos.DrawSphere(Utils.GridPositionToWordPosition(CollisionPosition), 0.01f);
            }
        }
    }
}
