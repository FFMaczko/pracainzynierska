﻿// Created by Filip Mączko
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    /// <summary>
    /// Struct storing data about single terrain destruction caused by an explosion.
    /// </summary>
    public readonly struct TerrainDestruction
    {
        public readonly int2 position;
        public readonly int radius;

        public TerrainDestruction(int2 position, int radius)
        {
            this.position = position;
            this.radius = radius;
        }
    }

    public class TerrainComponent : MonoBehaviour
    {
        public static List<TerrainComponent> objects = new List<TerrainComponent>();

        [Tooltip("Texture of map with alpha set to 0 on empty space.")]
        [SerializeField]
        private Texture2D defaultMask = default;

        public Texture2D terrainMask;

        public List<TerrainDestruction> terrainDestructions;


        public void CreateTerrainDestruction(int2 position, int width)
        {
            terrainDestructions.Add(new TerrainDestruction(position, width));
        }

        void Awake()
        {
            terrainDestructions = new List<TerrainDestruction>();
            terrainMask = GetComponent<SpriteMask>().sprite.texture;
            terrainMask.SetPixels(defaultMask.GetPixels());
            Utils.TerrainMapDimensions = new float2(terrainMask.width, terrainMask.height);
            terrainMask.Apply();
            transform.localScale = new Vector3(1, 1, 1);
        }

        private void OnEnable()
        {
            objects.Add(this);
        }

        private void OnDisable()
        {
            objects.Remove(this);
        }
    }
}
