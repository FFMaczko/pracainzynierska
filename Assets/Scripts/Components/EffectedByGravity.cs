﻿// Created by Filip Mączko

using System.Collections.Generic;
using UnityEngine;

namespace FM
{
    [RequireComponent(typeof(MoveableObject))]
    public class EffectedByGravity : MonoBehaviour
    {
        public static List<EffectedByGravity> objects = new List<EffectedByGravity>();

        /// <summary>
        /// Scale of gravity effecting this object.
        /// </summary>
        public float GravityScale = 1;

        /// <summary>
        /// Effected <see cref="FM.MoveableObject"/>.
        /// </summary>
        public MoveableObject MoveableObject;

        void Start()
        {
            
        }

        private void OnEnable()
        {
            MoveableObject = GetComponent<MoveableObject>();
            objects.Add(this);
        }

        private void OnDisable()
        {
            objects.Remove(this);
        }
    }
}
