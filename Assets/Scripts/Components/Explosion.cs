﻿// Created by Filip Mączko
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public static List<Explosion> objects = new List<Explosion>();
    public float Radius;
    public float ExplosionForce;
    public bool IsDeadly = true;

    public void SetSize(float radius)
    {
        float newSize = transform.localScale.x * radius / 100;
        Radius = radius / 100;
        transform.localScale = new Vector3(newSize, newSize, newSize);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

    private void NotDeadly()
    {
        IsDeadly = false;
    }

    private void Start()
    {
        transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));
    }

    private void OnEnable()
    {
        objects.Add(this);
    }

    private void OnDisable()
    {
        objects.Remove(this);
    }
}
