﻿// Created by Filip Mączko
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    public class MoveSystem : System
    {
        [Tooltip("Priority of given system.")]
        [SerializeField]
        private int priority = 95;
        public override void Execute()
        {
            ApplyVelocity();

        }

        public override int Priority()
        {
            return priority;
        }

        private void ApplyVelocity()
        {
            foreach(var obj in MoveableObject.objects)
            {
                //delete this line later
                obj.Velocity.x = Mathf.Clamp(obj.Velocity.x , - obj.MaxVelocity.x, obj.MaxVelocity.x);
                obj.Velocity.y = Mathf.Clamp(obj.Velocity.y, -obj.MaxVelocity.y, obj.MaxVelocity.y);
                obj.LastPosition = new float3(obj.PredictedPosition);
                obj.PredictedPosition += new float3(obj.Velocity.x * Time.deltaTime, obj.Velocity.y * Time.deltaTime, 0);
                obj.transform.position = obj.PredictedPosition;
            }
        }

        private void Start()
        {
            
        }
    }
}
