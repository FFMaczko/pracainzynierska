﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FM
{
    public class BulletCollisionsSystem : System
    {
        [SerializeField]
        private int priority = 200;
        public override void Execute()
        {
            List<int> indexesToRemove = new List<int>();
            for(int i=0;  i < Bullet.objects.Count; i++)
            {
                for (int j = 0; j < Bullet.objects.Count; j++)
                {
                    if(i != j && Vector3.Distance(Bullet.objects[i].transform.position, Bullet.objects[j].transform.position)< 0.125f)
                    {
                        if (!indexesToRemove.Contains(i))
                        {
                            indexesToRemove.Add(i);
                        }
                    }
                }
            }



            for (int i = Bullet.objects.Count - 1; i >=0 ; i--)
            {
                foreach(var player in Player.objects)
                {
                    if (Vector3.Distance(Bullet.objects[i].transform.position, player.transform.position) < 0.2f)
                    {
                        if (!indexesToRemove.Contains(i))
                        {
                            indexesToRemove.Add(i);
                        }
                    }
                }
            }

            for (int i = Bullet.objects.Count - 1; i >= 0; i--)
            {
                if (Bullet.objects[i].CollidablePoint.Collided)
                {
                    if (!indexesToRemove.Contains(i))
                    {
                        indexesToRemove.Add(i);
                    }
                }
            }

            indexesToRemove.Sort();

            for (int i= indexesToRemove.Count-1; i>=0; i--)
            {
                TerrainComponent.objects[0].CreateTerrainDestruction(Utils.WordPositionToGridPosition(Bullet.objects[indexesToRemove[i]].transform.position), Bullet.objects[indexesToRemove[i]].ExplosionRadius);
                Bullet.objects[indexesToRemove[i]].CreateExplosion();
                Destroy(Bullet.objects[indexesToRemove[i]].gameObject);
            }
            indexesToRemove.Clear();
        }

        public override int Priority()
        {
            return priority;
        }
    }
}
