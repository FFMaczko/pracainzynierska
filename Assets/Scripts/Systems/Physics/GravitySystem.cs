﻿// Created by Filip Mączko
using UnityEngine;

namespace FM
{
    public class GravitySystem : System
    {
        public const float GravityConstant = 5;
        
        [Tooltip("Priority of given system.")]
        [SerializeField]
        private int priority = 90;
        #region System
        public override void Execute()
        {
            ApplyGravity();
        }

        public override int Priority()
        {
            return priority;
        }
        #endregion

        private void ApplyGravity()
        {
            foreach (var obj in EffectedByGravity.objects)
            {
                obj.MoveableObject.Velocity.y -= obj.GravityScale * GravityConstant * Time.deltaTime;
            }
        }

        private void Start()
        {
            
        }
    }
}
