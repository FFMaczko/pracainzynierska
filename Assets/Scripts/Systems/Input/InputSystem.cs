﻿// Created by Filip Mączko
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    public class InputSystem : System
    {
        public Bullet bulletPrefab;

        [SerializeField]
        private int priority = 10;
        public override void Execute()
        {
            for(int i=0; i < InputMenager.Input.PlayerInputButtons.Count; i++)
            {
                if (i < Player.objects.Count)
                {
                    var input = InputMenager.Input.PlayerInputButtons[i];

                    if (Player.objects[i].JumpDelay > 10 && Input.GetButton(input.JumpButtonName))
                    {
                        Player.objects[i].Jump = true;
                        Player.objects[i].JumpDelay = 0;
                    }
                    else
                    {
                        Player.objects[i].Jump = false;
                    }
                    if (Player.objects[i].JumpDelay >=2)
                    {
                        Player.objects[i].PlayerVelocity.x = Input.GetAxis(input.MovementAxisName) * Player.PlayerMovementSpeed;
                        float2 velocity = Player.objects[i].MoveableObject.Velocity;
                        velocity.x = Input.GetAxis(input.MovementAxisName) * Player.PlayerMovementSpeed;
                        Player.objects[i].MoveableObject.Velocity = velocity;
                    }
                    if (Input.GetAxis(input.AimXAxisName) != 0 || Input.GetAxis(input.AimYAxisName) != 0)
                    {
                        Player.objects[i].AimArrow.up = new Vector3(Input.GetAxis(input.AimXAxisName), Input.GetAxis(input.AimYAxisName), 0).normalized;
                    }

                    if (Player.objects[i].ShootDelay >= 20 && Input.GetButton(input.ShootButtonName))
                    {
                        Player.objects[i].ShootDelay = 0;
                        var bullet = Instantiate(bulletPrefab, Player.objects[i].Barrel.position, Quaternion.Euler(Player.objects[i].Barrel.transform.up));
                        bullet.StartingSpeed = new float2(Player.objects[i].Barrel.transform.up.x, Player.objects[i].Barrel.transform.up.y) * Bullet.BulletSpeed;
                        bullet.Color = Player.objects[i].PlayerColor;
                    }
                    Player.objects[i].ShootDelay += 1;
                    Player.objects[i].JumpDelay += 1;
                }

            }
        }

        public override int Priority()
        {
            return priority;
        }
    }
}
