﻿// Created by Filip Mączko
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    public class PlayerInteractionsSystem : System
    {
        [SerializeField]
        private int priority;

        public override void Execute()
        {
            foreach (var obj in Player.objects)
            {
                if (obj.CollidablePoint.Collided)
                {
                    if (obj.Jump)
                    {
                        Jump(obj);
                        CorrectHeadPossition(obj);
                    }
                    else
                    {
                        if (!obj.Grounded)
                        {
                            CorrectDownPosition(obj);
                        }
                        MoveCollidingPlayer(obj);
                    }
                }
                else
                {
                    PlayerNotColliding(obj);
                    CorrectHeadPossition(obj);
                }
            }
        }

        public override int Priority()
        {
            return priority;
        }

        private void Jump(Player obj)
        {
            float deltaTime = Time.deltaTime;
            if (CanJump(obj,deltaTime))
            {
                obj.Grounded = false;
                obj.CollidablePoint.Collided = false;
                obj.MoveableObject.Velocity = new float2(0, obj.JumpSpeed);
                obj.MoveableObject.enabled = true;
                obj.EffectedByGravity.enabled = true;
                obj.Jump = false;
                obj.transform.position = obj.transform.position + new Vector3(obj.MoveableObject.Velocity.x, obj.MoveableObject.Velocity.y, 0) * deltaTime;
                obj.CollidablePoint.LastPosition = new float3(obj.CollidablePoint.CurrentPosition);
                obj.CollidablePoint.CurrentPosition = new float3(obj.CollidablePoint.transform.position);
            }
        }

        private bool CanJump(Player obj, float deltaTime)
        {
            Vector3 lastPosition = obj.transform.position;
            obj.transform.position = obj.transform.position + new Vector3(obj.MoveableObject.Velocity.x, obj.MoveableObject.Velocity.y, 0) * deltaTime;
            int2 coordinatesAfterJump = Utils.WordPositionToGridPosition(obj.HeadPoint.position);
            Color pixelUp = TerrainComponent.objects[0].terrainMask.GetPixel(coordinatesAfterJump.x, coordinatesAfterJump.y);
            obj.transform.position = lastPosition;
            return pixelUp.a == 0;
        }

        private void PlayerNotColliding(Player obj)
        {
            if (obj.Grounded)
            {
                obj.MoveableObject.Velocity += new float2(obj.PlayerVelocity.x, obj.PlayerVelocity.y);
                obj.Grounded = false;
            }
            obj.CollidablePoint.LastPosition = new float3(obj.CollidablePoint.CurrentPosition);
            obj.CollidablePoint.CurrentPosition = new float3(obj.CollidablePoint.transform.position);
            obj.MoveableObject.enabled = true;
            obj.EffectedByGravity.enabled = true;
        }

        private void MoveCollidingPlayer(Player obj)
        {
            int2 collisionPossition = obj.CollidablePoint.CollisionPosition;
            int2 movedPossition = Utils.WordPositionToGridPosition(Utils.GridPositionToWordPosition(collisionPossition) + obj.PlayerVelocity * Time.deltaTime);
            int minX = math.clamp(math.min(collisionPossition.x, movedPossition.x), 0, TerrainComponent.objects[0].terrainMask.width - 1);
            int width = math.abs(collisionPossition.x - movedPossition.x);
            if (math.min(collisionPossition.x, movedPossition.x) < 0)
            {
                width += math.min(collisionPossition.x, movedPossition.x);
            }
            if (math.max(collisionPossition.x, movedPossition.x) > TerrainComponent.objects[0].terrainMask.width - 1)
            {
                width = TerrainComponent.objects[0].terrainMask.width - 1 - math.min(collisionPossition.x, movedPossition.x);
            }
            int movedInY = 0;
            int movedInX = 0;
            int pointX = collisionPossition.x;
            for (int w = 1; w < width; w++)
            {

                if (collisionPossition.x < movedPossition.x)
                {
                    pointX += 1;
                }
                else
                {
                    pointX -= 1;
                }
                Color[] pixels = TerrainComponent.objects[0].terrainMask.GetPixels(pointX, math.max(0, collisionPossition.y - obj.PlayerHeight * 2), 1, obj.PlayerHeight * 4 + 1);

                int index = 3000;
                for (int i = 1; i <= 3 * obj.PlayerHeight; i++)
                {
                    if (pixels[i].a == 0 && pixels[i - 1].a != 0)
                    {
                        bool gapBigEnought = true;
                        for (int j = 0; j < obj.PlayerHeight; j++)
                        {
                            gapBigEnought &= pixels[i + j].a == 0;
                        }
                        if (gapBigEnought)
                        {
                            index = -2 * obj.PlayerHeight + i - 1;
                            break;
                        }
                    }

                }
                if (index != 3000)
                {
                    if (index > obj.MaxClimingDistance)
                    {
                        break;
                    }
                    movedInY = index;
                    if (movedPossition.x > collisionPossition.x)
                    {
                        movedInX++;
                    }
                    else
                    {
                        movedInX--;
                    }
                }
                else
                {
                    break;
                }
            }

            obj.Grounded = true;
            obj.MoveableObject.Velocity = float2.zero;
            obj.EffectedByGravity.enabled = false;
            obj.transform.position = Utils.GridPositionToWordPosition(new int2(collisionPossition.x + movedInX, collisionPossition.y + movedInY));

            if (movedInX == 0)
            {
                if (collisionPossition.x < movedPossition.x)
                {
                    minX = math.min(collisionPossition.x + 2, TerrainComponent.objects[0].terrainMask.width - 1);
                }
                else
                {
                    minX = math.max(0, collisionPossition.x - 2);

                }
                if (width > 2)
                {
                    Color[] allPixels = TerrainComponent.objects[0].terrainMask.GetPixels(minX, math.max(0,collisionPossition.y - obj.PlayerHeight * 2), width - 2, obj.PlayerHeight * 4 + 1);
                    bool noCollisions = true;
                    for (int j = 0; j < allPixels.Length; j++)
                    {
                        noCollisions &= allPixels[j].a == 0;
                    }
                    if (noCollisions)
                    {
                        PlayerNotColliding(obj);
                        obj.transform.position = Utils.GridPositionToWordPosition(movedPossition);
                    }
                }
                else
                {
                    PlayerNotColliding(obj);
                    obj.transform.position = Utils.GridPositionToWordPosition(movedPossition);
                }
            }
            obj.MoveableObject.LastPosition = new float3(obj.MoveableObject.PredictedPosition);
            obj.MoveableObject.PredictedPosition = new float3(obj.transform.position);
            obj.CollidablePoint.LastPosition = new float3(obj.CollidablePoint.CurrentPosition);
            obj.CollidablePoint.CurrentPosition = new float3(obj.CollidablePoint.transform.position);
            obj.CollidablePoint.Collided = false;

        }

        public void CorrectHeadPossition(Player obj)
        {
            int2 currentHeadPosition = Utils.WordPositionToGridPosition(obj.HeadPoint.position);
            int currentX = Utils.WordPositionToGridPosition(obj.transform.position).x;
            int currentY = Utils.WordPositionToGridPosition(obj.transform.position).y;
            if (currentX >= 0 && currentY >= 0 && currentX < TerrainComponent.objects[0].terrainMask.height && currentY < TerrainComponent.objects[0].terrainMask.height)
            {
                Color[] allPixelsInColumn = TerrainComponent.objects[0].terrainMask.GetPixels(currentHeadPosition.x, 0, 1, TerrainComponent.objects[0].terrainMask.height);
                if (currentHeadPosition.y + 1 < TerrainComponent.objects[0].terrainMask.height && allPixelsInColumn[currentHeadPosition.y + 1].a != 0)
                {
                    int correction = 0;
                    while (currentY - correction >= 0 && allPixelsInColumn[currentHeadPosition.y - correction].a != 0 && allPixelsInColumn[currentY - correction].a == 0)
                    {
                        correction += 1;
                    }
                    obj.transform.position = Utils.GridPositionToWordPosition(new int2(currentX, currentY - correction));
                    if (obj.MoveableObject.Velocity.y > 0)
                    {
                        obj.MoveableObject.Velocity.y = 0;
                    }
                }

                obj.MoveableObject.LastPosition = new float3(obj.MoveableObject.PredictedPosition);
                obj.MoveableObject.PredictedPosition = new float3(obj.transform.position);
                obj.CollidablePoint.LastPosition = new float3(obj.CollidablePoint.CurrentPosition);
                obj.CollidablePoint.CurrentPosition = new float3(obj.CollidablePoint.transform.position);
            }
        }

        public void CorrectDownPosition(Player obj)
        {
            int currentX = Utils.WordPositionToGridPosition(obj.transform.position).x;
            int currentY = Utils.WordPositionToGridPosition(obj.transform.position).y;
            if (currentX >= 0 && currentY >= 0 && currentX < TerrainComponent.objects[0].terrainMask.height && currentY < TerrainComponent.objects[0].terrainMask.height)
            {
                for (int i = 0; i <= obj.PlayerHeight; i++)
                {
                    Color[] allPixelsInColumn = TerrainComponent.objects[0].terrainMask.GetPixels( math.min(currentX+i, TerrainComponent.objects[0].terrainMask.width - 1), 0, 1, TerrainComponent.objects[0].terrainMask.height);
                    if (allPixelsInColumn[currentY].a != 0)
                    {
                        int correction = 1;
                        while (currentY + correction < TerrainComponent.objects[0].terrainMask.height && allPixelsInColumn[currentY + correction].a != 0)
                        {
                            correction += 1;
                        }

                        if (correction <= obj.PlayerHeight * 2)
                        {
                            obj.CollidablePoint.CollisionPosition = new int2(currentX, currentY + correction);
                            obj.transform.position = Utils.GridPositionToWordPosition(new int2(currentX, currentY + correction));
                            obj.MoveableObject.LastPosition = new float3(obj.MoveableObject.PredictedPosition);
                            obj.MoveableObject.PredictedPosition = new float3(obj.transform.position);
                            obj.CollidablePoint.LastPosition = new float3(obj.CollidablePoint.CurrentPosition);
                            obj.CollidablePoint.CurrentPosition = new float3(obj.CollidablePoint.transform.position);
                            return;
                        }
                    }
                }

                for (int i = 0; i <= obj.PlayerHeight; i++)
                {
                    Color[] allPixelsInColumn = TerrainComponent.objects[0].terrainMask.GetPixels(math.max(currentX - i, 0), 0, 1, TerrainComponent.objects[0].terrainMask.height);
                    if (allPixelsInColumn[currentY].a != 0)
                    {
                        int correction = 1;
                        while (currentY + correction < TerrainComponent.objects[0].terrainMask.height && allPixelsInColumn[currentY + correction].a != 0)
                        {
                            correction += 1;
                        }

                        if (correction <= obj.PlayerHeight * 2)
                        {
                            obj.CollidablePoint.CollisionPosition = new int2(currentX, currentY + correction);
                            obj.transform.position = Utils.GridPositionToWordPosition(new int2(currentX, currentY + correction));
                            obj.MoveableObject.LastPosition = new float3(obj.MoveableObject.PredictedPosition);
                            obj.MoveableObject.PredictedPosition = new float3(obj.transform.position);
                            obj.CollidablePoint.LastPosition = new float3(obj.CollidablePoint.CurrentPosition);
                            obj.CollidablePoint.CurrentPosition = new float3(obj.CollidablePoint.transform.position);
                            return;
                        }
                    }
                }
            }

        }

        private void Start()
        {

        }
    }
}
