﻿// Created by Filip Mączko
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    /// <summary>
    /// System responsible for checking collisions between <see cref="CollidablePoint"/>s and <see cref="TerrainComponent"/>.
    /// </summary>
    public class CollisionSystem : System
    {
        [Tooltip("Priority of given system.")]
        [SerializeField]
        private int priority = 102;

        private int gridHeight;
        private int gridWidth;

        public override void Execute()
        {
            foreach (var obj in CollidablePoint.objects)
            {
                obj.LastPosition = new float3(obj.CurrentPosition);
                obj.CurrentPosition = new float3(obj.transform.position);
                if (obj.UseLineCollisions)
                {
                    DetectLineCollision(obj);
                }
                else
                {
                    DetectPointCollision(obj);
                }

            }
        }

        private void DetectPointCollision(CollidablePoint collidablePoint)
        {
            int2 currentPosition = Utils.WordPositionToGridPosition(collidablePoint.CurrentPosition);
            int x = currentPosition.x;
            int y = currentPosition.y;
            if (!(y >= 0 && x < gridHeight && x >= 0 && y < gridWidth))
            {
                return;
            }
            Color pixelToCheck = TerrainComponent.objects[0].terrainMask.GetPixel(x, y);
            if (pixelToCheck.a != 0)
            {
                collidablePoint.CollisionPosition = new int2(x, y);
                collidablePoint.Collided = true;
            }
        }

        private void DetectLineCollision(CollidablePoint collidablePoint)
        {
            int2 LastPosition = Utils.WordPositionToGridPosition(collidablePoint.LastPosition);
            int2 CurrentPosition = Utils.WordPositionToGridPosition(collidablePoint.CurrentPosition);
            int x = math.min(LastPosition.x, CurrentPosition.x);
            int deltaX = math.abs(LastPosition.x - CurrentPosition.x) + 1;
            int y = math.min(LastPosition.y, CurrentPosition.y);
            int deltaY = math.abs(LastPosition.y - CurrentPosition.y) + 1;

            if (!(y >= 0 && x + deltaX < gridHeight && x >= 0 && y + deltaY < gridWidth))
            {
                return;
            }
            Color[] pixelsToCheck = TerrainComponent.objects[0].terrainMask.GetPixels(x, y, deltaX, deltaY);


            int a = deltaY / deltaX;
            int a2 = a + (int)math.sign(LastPosition.y - CurrentPosition.y);
            if (CurrentPosition.x > LastPosition.x)
            {
                int index1 = 0;
                int index2 = 0;
                for (int i = 0; i <= deltaX; i++)
                {
                    index1 = i + i * a;
                    index2 = i + i * a2;
                    if (index1 < pixelsToCheck.Length && pixelsToCheck[index1].a != 0)
                    {
                        collidablePoint.CollisionPosition = new int2(x + i, y + a * i);
                        collidablePoint.Collided = true;
                        return;
                    }
                    if (index2 < pixelsToCheck.Length && pixelsToCheck[index2].a != 0)
                    {
                        collidablePoint.CollisionPosition = new int2(x + i, y + a2 * i);
                        collidablePoint.Collided = true;
                        return;
                    }
                }
            }
            else if (CurrentPosition.x < LastPosition.x)
            {
                int index1 = 0;
                int index2 = 0;
                for (int i = deltaX; i >= 0; i--)
                {
                    index1 = i + i * a;
                    index2 = i + i * a2;
                    if (index1 < pixelsToCheck.Length && pixelsToCheck[index1].a != 0)
                    {
                        collidablePoint.CollisionPosition = new int2(x + i, y + a * i);
                        collidablePoint.Collided = true;
                        return;
                    }
                    if (index2 < pixelsToCheck.Length && pixelsToCheck[index2].a != 0)
                    {
                        collidablePoint.CollisionPosition = new int2(x + i, y + a2 * i);
                        collidablePoint.Collided = true;
                        return;
                    }
                }
            }
            else
            {
                if (LastPosition.y > CurrentPosition.y)
                {
                    for (int i = deltaY; i >= 0; i--)
                    {
                        if (i < pixelsToCheck.Length && pixelsToCheck[i].a != 0)
                        {
                            collidablePoint.CollisionPosition = new int2(x, y + i);
                            collidablePoint.Collided = true;
                            return;
                        }
                    }
                }
                else
                {
                    for (int i = deltaY; i >= 0; i--)
                    {
                        if (i < pixelsToCheck.Length && pixelsToCheck[i].a != 0)
                        {
                            collidablePoint.CollisionPosition = new int2(x, y + i);
                            collidablePoint.Collided = true;
                            return;
                        }
                    }
                }
            }
        }

        public override int Priority()
        {
            return priority;
        }

        private void Start()
        {
            gridHeight = TerrainComponent.objects[0].terrainMask.height;
            gridWidth = TerrainComponent.objects[0].terrainMask.width;
        }
    }
}
