﻿// Created by Filip Mączko

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    /// <summary>
    /// System responsible for terrain destruction.
    /// </summary>
    public class TerrainSystem : System
    {
        public static readonly Color DestroyedColor = new Color(0, 0, 0, 0);

        [Tooltip("Priority of given system.")]
        [SerializeField]
        private int priority = 100;

        private Color[] destructionColors;

        #region System
        public override int Priority()
        {
            return priority;
        }

        public override void Execute()
        {
            foreach (var obj in TerrainComponent.objects)
            {
                UpdateTerrainMask(obj);
            }
        }
        #endregion

        private void LoadDefaultMask()
        {
            destructionColors = new Color[TerrainComponent.objects[0].terrainMask.width * TerrainComponent.objects[0].terrainMask.height];
            for (int i = 0; i < destructionColors.Length; i++)
            {
                destructionColors[i] = DestroyedColor;
            }
        }


        private void UpdateTerrainMask(TerrainComponent terrainComponent)
        {
            for (int i = 0; i < terrainComponent.terrainDestructions.Count; i++)
            {
                if (terrainComponent.terrainDestructions[i].radius > 0)
                {
                    CutCircle(terrainComponent.terrainDestructions[i].position, terrainComponent.terrainDestructions[i].radius, terrainComponent);
                    terrainComponent.terrainDestructions[i] = new TerrainDestruction(new int2(), -1);
                }
            }
            terrainComponent.terrainMask.Apply();
        }


        private void CutCircle(int2 position, int radius, TerrainComponent terrainComponent)
        {
            int currentHeight = 0;
            int currentWidht = radius;
            foreach (var rect in ListOfRectanglesInCircle(radius))
            {
                currentWidht = rect.x;
                CutRectFromMap(new int2(position.x - currentWidht / 2, position.y + currentHeight), new int2(currentWidht, rect.y), terrainComponent);
                CutRectFromMap(new int2(position.x - currentWidht / 2, position.y - currentHeight - rect.y), new int2(currentWidht, rect.y), terrainComponent);
                currentHeight += rect.y;
            }
        }

        private void CutRectFromMap(int2 position, int2 dimensions, TerrainComponent terrainComponent)
        {
            int2 rectPosition = position;
            int2 rectSize = dimensions;
            if (position.x < 0)
            {
                rectPosition.x = 0;
                rectSize.x += position.x;
            }
            if (position.y < 0)
            {
                rectPosition.y = 0;
                rectSize.y += position.y;
            }
            if (position.x + dimensions.x >= terrainComponent.terrainMask.width)
            {
                rectSize.x = terrainComponent.terrainMask.width - position.x;
            }
            if (position.y + dimensions.y >= terrainComponent.terrainMask.height)
            {
                rectSize.y = terrainComponent.terrainMask.height - position.y;
            }

            if (rectSize.x > 0 && rectSize.y > 0)
            {
                terrainComponent.terrainMask.SetPixels(rectPosition.x, rectPosition.y, rectSize.x, rectSize.y, destructionColors);
            }
        }
        private List<int2> ListOfRectanglesInCircle(int radius)
        {
            List<int2> rectangles = new List<int2>();
            float currentWidth = radius;
            float currentHeight = 0;
            float heightSum = 0;
            while (heightSum < radius)
            {
                if (radius * radius >= currentWidth * currentWidth + heightSum * heightSum)
                {
                    currentHeight += 1;
                    heightSum += 1;
                }
                else
                {
                    if (currentHeight > 0)
                    {
                        rectangles.Add(new int2((int)currentWidth * 2, (int)currentHeight));
                    }
                    currentHeight = 0;
                    currentWidth -= 1;
                }

            }
            if (currentHeight > 0)
            {
                rectangles.Add(new int2((int)currentWidth * 2, (int)currentHeight));
            }
            return rectangles;
        }

        #region unity functions
        private void Start()
        {
            LoadDefaultMask();
        }
        #endregion
    }
}
