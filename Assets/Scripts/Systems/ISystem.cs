﻿// Created by Filip Mączko
using System;
using UnityEngine;

namespace FM
{
    /// <summary>
    /// Abstract class that all systems will derive from.
    /// </summary>
    public abstract class System : MonoBehaviour, IComparable<System>
    {
        #region IComparable
        public int CompareTo(System other)
        {
            return Priority() - other.Priority();
        }
        #endregion

        #region System Functions
        /// <summary>
        /// Priority of given system.
        /// </summary>
        /// <returns></returns>
        public abstract int Priority();

        /// <summary>
        /// Method that will be executed every frame.
        /// </summary>
        public abstract void Execute();
        #endregion
    }
}
