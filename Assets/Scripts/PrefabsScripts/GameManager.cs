﻿using UnityEngine;
using UnityEngine.UI;
namespace FM
{
    public class GameManager : System
    {
        [SerializeField]
        private int priority = 1000;

        public Transform[] playerSpawns;
        public Player[] playersPrefabs;
        public Canvas EndGameCanvas;
        public Text Winner;

        private void Start()
        {
            EndGameCanvas.enabled = false;
            for (int i=0; i< InputMenager.Input.PlayerInputButtons.Count; i++)
            {
                Instantiate(playersPrefabs[i], playerSpawns[i].position, playerSpawns[i].rotation);
            }
        }

        public override void Execute()
        {
            for(int i= Player.objects.Count-1;  i>=0; i--)
            {
                if(Player.objects[i].transform.position.y < -10.5f)
                {
                    if (TestData.Data == null)
                    {
                        InputMenager.Input.PlayerInputButtons.RemoveAt(i);
                    }
                    Destroy(Player.objects[i].gameObject);
                    continue;
                }
                foreach(var explosion in Explosion.objects)
                {
                    if(explosion.IsDeadly && Vector3.Distance(Player.objects[i].transform.position, explosion.transform.position) < explosion.Radius)
                    {
                        if (TestData.Data == null)
                        {
                            InputMenager.Input.PlayerInputButtons.RemoveAt(i);
                        }
                        Destroy(Player.objects[i].gameObject);
                    }
                }
            }

            if(Player.objects.Count == 0)
            {
                EndGame();
            }
            if (Player.objects.Count == 1)
            {
                EndGame();
                Winner.color = Player.objects[0].PlayerColor;
                Winner.text = $"Player: {Player.objects[0].ID + 1} won";
            }
        }

        private void EndGame()
        {
            EndGameCanvas.enabled = true;
            GameObject.Find("Solver").SetActive(false);
        }

        public override int Priority()
        {
            return priority;
        }
    }
}
