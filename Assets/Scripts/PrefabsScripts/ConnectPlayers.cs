﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace FM
{
    public class ConnectPlayers : MonoBehaviour
    {
        public Image[] images;
        public Color[] colors;
        public Text[] text;

        private void Start()
        {
            InputMenager.Input.PlayerInputButtons.Clear();
        }

        void Update()
        {
            if (Input.GetButton("Shoot0") && !InputAlreadyAdded("Shoot0"))
            {
                text[0].text = "Connected";
                images[0].color = colors[InputMenager.Input.PlayerInputButtons.Count];
                InputMenager.Input.PlayerInputButtons.Add(
    new PlayerInputButtons()
    {
        MovementAxisName = "Movement0",
        AimXAxisName = "AimX0",
        AimYAxisName = "AimY0",
        JumpButtonName = "Jump0",
        ShootButtonName = "Shoot0"
    }
    );
            }
            if (Input.GetButton("Shoot1") && !InputAlreadyAdded("Shoot1"))
            {
                text[1].text = "Connected";
                images[1].color = colors[InputMenager.Input.PlayerInputButtons.Count];
                InputMenager.Input.PlayerInputButtons.Add(
    new PlayerInputButtons()
    {
        MovementAxisName = "Movement1",
        AimXAxisName = "AimX1",
        AimYAxisName = "AimY1",
        JumpButtonName = "Jump1",
        ShootButtonName = "Shoot1"
    }
    );
            }
            if (Input.GetButton("Shoot2") && !InputAlreadyAdded("Shoot2"))
            {
                text[2].text = "Connected";
                images[2].color = colors[InputMenager.Input.PlayerInputButtons.Count];
                InputMenager.Input.PlayerInputButtons.Add(
new PlayerInputButtons()
{
    MovementAxisName = "Movement2",
    AimXAxisName = "AimX2",
    AimYAxisName = "AimY2",
    JumpButtonName = "Jump2",
    ShootButtonName = "Shoot2"
}
);
            }
            if (Input.GetButton("Shoot3") && !InputAlreadyAdded("Shoot3"))
            {
                text[3].text = "Connected";
                images[3].color = colors[InputMenager.Input.PlayerInputButtons.Count];
                InputMenager.Input.PlayerInputButtons.Add(
new PlayerInputButtons()
{
    MovementAxisName = "Movement3",
    AimXAxisName = "AimX3",
    AimYAxisName = "AimY3",
    JumpButtonName = "Jump3",
    ShootButtonName = "Shoot3"
}
);
            }

        }

        private bool InputAlreadyAdded(string s)
        {
            foreach (var v in InputMenager.Input.PlayerInputButtons)
            {
                if (s.Equals(v.ShootButtonName))
                {
                    return true;
                }
            }
            return false;
        }
    }


}
