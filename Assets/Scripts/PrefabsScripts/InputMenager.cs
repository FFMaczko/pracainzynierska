﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FM
{
    public struct PlayerInputButtons
    {
        public string MovementAxisName;
        public string AimXAxisName;
        public string AimYAxisName;
        public string JumpButtonName;
        public string ShootButtonName;
    }

    public class InputMenager : MonoBehaviour
    {
        public static InputMenager Input;
        public List<PlayerInputButtons> PlayerInputButtons = new List<PlayerInputButtons>();

        void Awake()
        {
            if(Input == null)
            {
                Input = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

    }
}
