﻿// Created by Filip Mączko
using Unity.Mathematics;
using UnityEngine;

namespace FM
{
    /// <summary>
    /// Class with methods shared by multiple systems and components.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Dimensions in pixels of TerrainMask.
        /// </summary>
        public static float2 TerrainMapDimensions = default;
        
        /// <summary>
        /// Scaling factor of TerrainMask.
        /// </summary>
        public static float ScaleingFactor = 100;

        /// <summary>
        /// Function returning grid position given word position.
        /// </summary>
        /// <param name="wordPosition"> Vector3 representing word position of an object. </param>
        /// <returns> Grid position of an object. </returns>
        public static int2 WordPositionToGridPosition(float3 wordPosition)
        {
            var scaledPosition = wordPosition * ScaleingFactor;
            var positionCorrections = TerrainMapDimensions * 0.5f;
            scaledPosition += new float3(positionCorrections,0);
            var gridPosition = new int2((int)Mathf.Round(scaledPosition.x), (int)Mathf.Round(scaledPosition.y));
            return gridPosition;
        }

        /// <summary>
        /// Function returning word position from a given wordPosition <see cref="Vector3"/>
        /// </summary>
        /// <param name="gridPosition"></param>
        /// <returns></returns>
        public static Vector3 GridPositionToWordPosition(int2 gridPosition)
        {
            Vector3 wordPosition = new Vector3(gridPosition.x / ScaleingFactor, gridPosition.y / ScaleingFactor, 0);
            Vector3 wordPositionCorecction = new Vector3(-TerrainMapDimensions.x/(2*ScaleingFactor), -TerrainMapDimensions.y/(2*ScaleingFactor), 0);
            wordPosition += wordPositionCorecction;
            return wordPosition;
        }
    }
}
