﻿// Created by Filip Mączko
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FM
{
    public class LoopSolver : MonoBehaviour
    {
        private List<System> systems;

        private void ExecuteSystems()
        {
            foreach (System system in systems)
            {
                if (system.isActiveAndEnabled)
                {
                    system.Execute();
                }
            }
        }

        #region unity functions

        void Start()
        {
            Application.targetFrameRate = 144;
            systems = GameObject.FindObjectsOfType<System>().ToList();
            systems.Sort();
        }

        private void Update()
        {
            ExecuteSystems();
        }
        #endregion
    }
}
