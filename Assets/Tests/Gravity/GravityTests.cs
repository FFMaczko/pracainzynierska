﻿// Created by Filip Mączko
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace FM.Tests
{
    public class GravityTests
    {
        private string scene = "GravityTests";

        [UnityTest]
        public IEnumerator LineCollisionsDetected()
        {
            yield return SceneManager.LoadSceneAsync(scene);
            yield return new WaitUntil(() => SceneManager.GetActiveScene().name == scene);
            float startingPosition;
            float startingVelocity;
            yield return new WaitForSeconds(0.1f);
            MoveableObject moveableObject = GameObject.FindObjectOfType<MoveableObject>(includeInactive: true);
            moveableObject.gameObject.SetActive(true);
            startingPosition = moveableObject.transform.position.y;
            startingVelocity = moveableObject.Velocity.y;
            yield return new WaitForSeconds(0.5f);
            Assert.Less(moveableObject.transform.position.y, startingPosition);
            Assert.Less(moveableObject.Velocity.y, startingPosition);
            Assert.GreaterOrEqual(moveableObject.Velocity.y, -moveableObject.MaxVelocity.y);
        }
    }
}