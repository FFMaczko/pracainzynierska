﻿// Created by Filip Mączko
using NUnit.Framework;
using System.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace FM.Tests
{
    public class TerrainDestructionTests
    {
        private string scene = "TerrainDestructionTest";
        private Vector3[] collisions = {
        new Vector3(-9.35f, 1.17f,0),
        new Vector3(-4.49f, -3.88f,0),
        new Vector3(-4.65f, 4.06f,0),
        new Vector3(8.67f, -3.39f,0)
        };

        [UnityTest]
        public IEnumerator TerrainDestructed()
        {
            yield return SceneManager.LoadSceneAsync(scene);
            yield return new WaitUntil(() => SceneManager.GetActiveScene().name == scene);
            yield return new WaitForSeconds(0.1f);
            CollidablePoint[] collidableObjects = GameObject.FindObjectsOfType<CollidablePoint>(includeInactive: true);
            foreach (var obj in collidableObjects)
            {
                obj.gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(1f);

            foreach(var collision in collisions)
            {
                int2 collisionOnGrid = Utils.WordPositionToGridPosition(collision);
                Assert.AreEqual(TerrainComponent.objects[0].terrainMask.GetPixel(collisionOnGrid.x, collisionOnGrid.y).a, 0);
            }
        }
    }
}
