﻿// Created by Filip Mączko
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace FM.Tests
{
    public class CollisionSystemTestScript
    {
        private string scene = "CollisionTests";

        [UnityTest]
        public IEnumerator LineCollisionsDetected()
        {
            yield return SceneManager.LoadSceneAsync(scene);
            yield return new WaitUntil(() => SceneManager.GetActiveScene().name == scene);
            yield return new WaitForSeconds(0.1f);
            CollidablePoint[] collidableObjects = GameObject.FindObjectsOfType<CollidablePoint>(includeInactive: true);
            foreach (var obj in collidableObjects)
            {
                obj.gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(1);
            foreach (var obj in CollidablePoint.objects)
            {
                if (obj.UseLineCollisions)
                {
                    Assert.True(obj.Collided);
                }
            }
        }
    }
}
