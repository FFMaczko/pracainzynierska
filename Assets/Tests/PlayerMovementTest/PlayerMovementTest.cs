﻿// Created by Filip Mączko
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace FM.Tests
{
    public class PlayerMovementTest
    {
        private string scene = "PlayerMovementTest";

        [UnityTest]
        public IEnumerator SimpleWalk()
        {
            yield return SceneManager.LoadSceneAsync(scene);
            yield return new WaitUntil(() => SceneManager.GetActiveScene().name == scene);
            var player = GameObject.Find("Player(1)");
            player.GetComponent<Player>().enabled = true;
            player.GetComponent<MoveableObject>().enabled = true;
            player.GetComponent<EffectedByGravity>().enabled = true;
            player.SetActive(true);
            yield return new WaitForSeconds(10f);
            Assert.Greater(player.transform.position.x, 5);
        }

        [UnityTest]
        public IEnumerator ToNarowPath()
        {
            yield return SceneManager.LoadSceneAsync(scene);
            yield return new WaitUntil(() => SceneManager.GetActiveScene().name == scene);
            var player = GameObject.Find("Player(2)");
            player.GetComponent<Player>().enabled = true;
            player.GetComponent<MoveableObject>().enabled = true;
            player.GetComponent<EffectedByGravity>().enabled = true;
            player.SetActive(true);
            yield return new WaitForSeconds(5f);
            Assert.Less(player.transform.position.x, 5);
        }

        [UnityTest]
        public IEnumerator ToStipHill()
        {
            yield return SceneManager.LoadSceneAsync(scene);
            yield return new WaitUntil(() => SceneManager.GetActiveScene().name == scene);
            var player = GameObject.Find("Player(3)");
            player.GetComponent<Player>().enabled = true;
            player.GetComponent<MoveableObject>().enabled = true;
            player.GetComponent<EffectedByGravity>().enabled = true;
            player.SetActive(true);
            yield return new WaitForSeconds(5f);
            Assert.Less(player.transform.position.x, 5);
        }

        [UnityTest]
        public IEnumerator WalkOtherWayWithJump()
        {
            yield return SceneManager.LoadSceneAsync(scene);
            yield return new WaitUntil(() => SceneManager.GetActiveScene().name == scene);
            var player = GameObject.Find("Player(4)");
            player.GetComponent<Player>().enabled = true;
            player.GetComponent<MoveableObject>().enabled = true;
            player.GetComponent<EffectedByGravity>().enabled = true;
            player.SetActive(true);
            yield return new WaitForSeconds(2f);
            player.GetComponent<Player>().Jump = true;
            float yBeforeJump = player.transform.position.y;
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            var velocity = player.GetComponent<MoveableObject>().Velocity;
            velocity.x = -5;
            player.GetComponent<MoveableObject>().Velocity = velocity;
            yield return new WaitForSeconds(0.5f);
            float yMidJump = player.transform.position.y;
            yield return new WaitForSeconds(2.5f);
            Assert.Greater(yMidJump, yBeforeJump);
            Assert.Less(player.transform.position.x, -5);
        }
    }
}